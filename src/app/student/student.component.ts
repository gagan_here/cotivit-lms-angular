import { Component, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Student } from '../student';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  students: any;

  constructor(private service:StudentService,
    private router: Router) { }

  ngOnInit(): void {
    this.getLatestStudents();
  }

  getLatestStudents() {
    let resp = this.service.getStudents();
    resp.subscribe((data)=>this.students=data);
    console.log(this.students);
    
  }

  editStudent(id: number) {
    console.log(id);
    this.router.navigate(['update', id]);
  }

  deleteStudent(id: number) {
    let resp = this.service.deleteStudent(id);
    resp.subscribe(data => {
      console.log(data);
      
      this.getLatestStudents();
    });
  }

  findStudentByEmailId(emails: string) {
    this.router.navigate(['search', emails]);
  }

  addBook(studentid: number) {
    this.router.navigate(['books', studentid])
  }

}

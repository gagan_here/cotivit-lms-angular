import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentBookinfoComponent } from './student-bookinfo.component';

describe('StudentBookinfoComponent', () => {
  let component: StudentBookinfoComponent;
  let fixture: ComponentFixture<StudentBookinfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentBookinfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentBookinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

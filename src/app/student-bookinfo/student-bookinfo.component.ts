import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-student-bookinfo',
  templateUrl: './student-bookinfo.component.html',
  styleUrls: ['./student-bookinfo.component.css']
})
export class StudentBookinfoComponent implements OnInit {

  info: any;
  data: any

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    
    this.route.queryParams.subscribe((params) => {
      console.log(params);
      this.data = JSON.parse(params.data);
      this.info = this.data
      console.log(this.info);
    })
    
  }

}

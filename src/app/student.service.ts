import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Student } from './student';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http:HttpClient) { }

  createStudent(obj: Student) : Observable<Object>{
    return this.http.post("http://localhost:8080/students", obj, { responseType: 'text' });
  }

  public getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>("http://localhost:8080/students");
  }

  getStudentById(id: number): Observable<Student> {
    return this.http.get<Student>("http://localhost:8080/students/" + id);
  }

  getStudentByEmailId(email: string): Observable<Student> {
    return this.http.get<Student>("http://localhost:8080/student/" + email);
  }

  updateStudent(id: number, student: Student): Observable<Object> {
    return this.http.put("http://localhost:8080/students/" + id , student);
  }

  deleteStudent(id: number) {
    return this.http.delete("http://localhost:8080/students/" + id, { responseType: 'text' });
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StudentComponent } from './student/student.component';
import { HttpClientModule } from '@angular/common/http';
import { StudentService } from './student.service';
import { FormsModule } from '@angular/forms';
import { RegisterStudentComponent } from './register-student/register-student.component';
import { UpdateStudentComponent } from './update-student/update-student.component';
import { SearchStudentComponent } from './search-student/search-student.component';
import { BooksComponent } from './books/books.component';
import { StudentBookinfoComponent } from './student-bookinfo/student-bookinfo.component';

@NgModule({
  declarations: [
    AppComponent,
    StudentComponent,
    RegisterStudentComponent,
    UpdateStudentComponent,
    SearchStudentComponent,
    BooksComponent,
    StudentBookinfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [StudentService],
  bootstrap: [AppComponent]
})
export class AppModule { }

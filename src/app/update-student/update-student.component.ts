import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Student } from '../student';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-update-student',
  templateUrl: './update-student.component.html',
  styleUrls: ['./update-student.component.css']
})
export class UpdateStudentComponent implements OnInit {

  id: number
  alert: boolean = false;
  student: Student = new Student(0, "", "", "", "", "", "");

  constructor(private service: StudentService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {

    this.id = this.route.snapshot.params['id'];

    this.service.getStudentById(this.id).subscribe(data => {
      this.student = data;
      console.log(data);
      
    }, error => console.log(error));
  }

  onSubmit(userForm) {
    this.service.updateStudent(this.id, this.student).subscribe( data => {
      // this.goToStudentComponent();
      userForm.form.reset();
    }, error => console.log(error));

    this.alert = true;
  }

  goToStudentComponent() {
    this.router.navigate(['students']);
  }

  findStudentByEmailId(emails: string) {
    this.router.navigate(['search', emails]);
  }

  closeAlert() {
    this.alert=false;
  }
}

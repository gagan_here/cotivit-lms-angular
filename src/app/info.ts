export class Info {

    constructor(
        // student
        id: number, //id
        fn: string, //first name
        ln: string, // last name
        cn: string, // class name

        //book
        bn: string, //book name
        wr: string // writer
    ){}

  
}
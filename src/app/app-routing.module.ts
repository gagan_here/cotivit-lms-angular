import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksComponent } from './books/books.component';
import { RegisterStudentComponent } from './register-student/register-student.component';
import { SearchStudentComponent } from './search-student/search-student.component';
import { StudentBookinfoComponent } from './student-bookinfo/student-bookinfo.component';
import { StudentComponent } from './student/student.component';
import { UpdateStudentComponent } from './update-student/update-student.component';

const routes: Routes = [
  // {path:"", redirectTo:"register", pathMatch:"full"},
  {path:"students", component:StudentComponent},
  {path:"register", component:RegisterStudentComponent},
  {path:"update/:id", component:UpdateStudentComponent},
  {path:"search/:emails", component:SearchStudentComponent},
  {path:"books/:studentid", component:BooksComponent},
  {path: "info", component:StudentBookinfoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

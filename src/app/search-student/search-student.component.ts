import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Student } from '../student';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-search-student',
  templateUrl: './search-student.component.html',
  styleUrls: ['./search-student.component.css']
})
export class SearchStudentComponent implements OnInit {

  emails: string;
  student: Student = new Student(0, "", "", "", "", "", "");

  constructor(private service: StudentService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.emails=this.route.snapshot.params['emails'];
    console.log(this.emails);
    
    this.service.getStudentByEmailId(this.emails).subscribe(data => {
      this.student = data;
      console.log(data);
      
    }, error => console.log(error));
  }

  findStudentByEmailId(email: string) {
    this.service.getStudentByEmailId(email).subscribe(data => {
      this.student = data;
      console.log(data);
      
    }, error => console.log(error));
  }

}

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BookService } from '../book.service';
import { Books } from '../books';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  studentid: number;

  student: any;
  book: any;

  books: any

  // student
  id: number; //id
  fn: string; //first name
  ln: string; // last name
  cn: string; // class name

  //book
  bn: string; //book name
  wr: string; // writer

  @Output() sendToParent = new EventEmitter();

  constructor(private service: BookService,
    private studentService: StudentService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.studentid = this.route.snapshot.params['studentid'];
    this.getLatestBooks();

    let resp = this.studentService.getStudentById(this.studentid);


    resp.subscribe((data) => {
      this.student = data, 
      this.id = this.student.studentid;
      this.fn = this.student.firstname;
      this.ln = this.student.lastname;
      this.cn = this.student.classname;

      // this.info = new Info(this.id, this.fn, this.ln, this.cn, this.bn, this.wr);
      // console.log(this.info);
      

      console.log(this.fn);
    }
    
    );
    
  }

  getLatestBooks() {
    let resp = this.service.getBooks();
    resp.subscribe((data) => this.books=data);
  }


  // bookname:string,
  // writer:string,

  addBookToStudent(book: Books) {







    this.book = book;

    this.bn = this.book.bookname;
    this.wr = this.book.writer;

    let info = {...this.student, ...book}; 

            // // student
            // id: number, //id
            // fn: string, //first name
            // ln: string, // last name
            // cn: string, // class name
    
            // //book
            // bn: string, //book name
            // wr: string // writer

            delete info["email"]
            delete info["phone"]
            delete info["address"]
            delete info["bookid"]
            delete info["publication"]
            delete info["available"]
            delete info["student"]
            delete info["books"]

    console.log(info);
    

    // this.info = new Info(this.id, this.fn, this.ln, this.cn, this.bn, this.wr);

    // console.log(this.info);
    

    // console.log(this.bn);
    // console.log(this.wr);
    
    
    // console.log(this.book);
    // console.log(this.student);
    
    this.router.navigate(["info"], {
      queryParams: {info:JSON.stringify(info)}
    })
    
  }
  

}

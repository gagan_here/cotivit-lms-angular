import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Student } from '../student';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-register-student',
  templateUrl: './register-student.component.html',
  styleUrls: ['./register-student.component.css']
})
export class RegisterStudentComponent implements OnInit {

  student: Student = new Student(0, "", "", "", "", "", "")
  alert: boolean = false;

  constructor(private service:StudentService,
    private router: Router) { }

  ngOnInit(): void {
  }

  addUser(userForm) {
    let obj = userForm.value;
    this.service.createStudent(obj).subscribe(response => {
      userForm.form.reset();
    })

    this.alert = true;
  }

  findStudentByEmailId(emails: string) {
    this.router.navigate(['search', emails]);
  }

  closeAlert() {
    this.alert=false;
  }

}
